## Overlay folders

Put simply, several folders are overlaid. In the resulting folder, the
docker image is generated.

    component/template/flavour/default/
    for flavour in flavours:
        component/template/flavour/$flavour/
        secrets/template/flavour/$flavour/

The purpose of the folders is:

### component/template/
contains generic scripts and information reused by all components.

### secrets/
is not managed by git (listed in .gitignore) and can be used to store your credentials

## Docker build

In the newly created folder, all files within "files" folder are added to files.tar
