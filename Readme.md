# Jenkins and Monitoring

This repository contains a couple of recipes to generate pre-configured
jenkins-, nginx-, fluentd- and other docker images to set up a
modularized Jenkins CI infrastructure. The added value compared to the
available docker images is the management if different customized flavours,
alignment of components (Jenkins, Fluentd, Influxdb, Postgres and Grafana
will be aligned in a way that Grafana provides a relevant operations
dashboard, build information is stored in postgres for statistical
analysis. Nginx is configured to reduce load on Jenkins by caching
static contexts and re-directing some of the more expensive operations like
e.g. build trends, node build history etc. from Jenkins to a separate,
database-driven dashboard.

Jenkins is a great tool to schedule and orchestrate builds on distributed
build- and test-nodes. But especially when running with a high load,
it has some limitations when it comes to analyzing build histories
internally.

My view is that the Jenkins server and build history should be seen as
volatile. Data should be stored separately in a database.

## General Concept

### Files added to Docker image

Put simply, several folders are overlaid. In the resulting folder, the
docker image is generated.

    component/template/flavour/default/
    for flavour in flavours:
        component/template/flavour/$flavour/
        secrets/template/flavour/$flavour/

The purpose of the folders is:

#### component/template/
contains generic scripts and information reused by all components.

#### secrets/
is not managed by git (listed in .gitignore) and can be used to store your credentials
