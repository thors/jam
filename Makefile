
ROOT_FOLDER=$(shell pwd)
FRAMEWORK_PREFIX=jam
VERSION:=0.1
FLAVOUR:=default

include component/jenkins/Makefile
include component/nginx/Makefile
include component/gerrit/Makefile

help:
	@echo Help is provided separately per component.
	@echo To get help on how to build/start jenkins docker images, make help-jenkins
