#!/bin/sh -ex

if [ ! -e /etc/nginx/ssl ]; then
  mkdir -p /etc/nginx.ref/ssl
  cd /etc/nginx.ref/ssl
  /opt/bin/generate_self_signed_certificate.sh  ${JAM_DOMAIN_NAME:-jam}
fi

find /etc/nginx.ref/ -type d | sed "s/nginx.ref/nginx/" | while read d; do  mkdir -p $d; done;
find /etc/nginx.ref/ -type f | while read f_template; do
  f_target=$(echo $f_template | sed "s/nginx.ref/nginx/")
  if [ ! -f "$f_target" ]; then
    sed "s/\$JAM_DOMAIN_NAME/$JAM_DOMAIN_NAME/g" "$f_template" > "$f_target"
  fi
done

nginx -g 'daemon off;'

