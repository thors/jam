TEMPLATE:=8080
TEMPLATE_NAME:=jam
PERSISTENT_ROOT:=$(ROOT_FOLDER)/persistent
TEMPLATE_PERSISTENT:=$(PERSISTENT_ROOT)/$(TEMPLATE_NAME)
TEMPLATE_PROFILE:=local
TEMPLATE_ENV_FILE=$(ROOT_FOLDER)/component/template/profile/$(TEMPLATE_PROFILE).env

# Here we are building the docker image after collecting all necessary files
build-template-internal:
	cd $(BUILD_FOLDER)/files; \
		tar cf ../files.tar *
	cd $(BUILD_FOLDER); \
		docker build . -t $(FRAMEWORK_PREFIX)/template-$(FLAVOUR):$(VERSION)

# This is the default preparation for the build folder, to build the new 
# docker image. It cleans any pre-existing build-folder and sets some
# generally used variables
prepare-template-default:
	echo prepare template default...
	$(eval BUILD_FOLDER=$(ROOT_FOLDER)/build/template)
	$(eval FLAVOUR=default)
	rm -rf $(BUILD_FOLDER)
	mkdir -p $(BUILD_FOLDER)
	mkdir -p $(BUILD_FOLDER)/files
	cp -r $(ROOT_FOLDER)/component/template/flavour/default/* $(BUILD_FOLDER)

# This is an example for the preparation of the build-folder for a customized
# Jenkins server. To add your own flavour, you will need to copy this
# receipe and adapt it.
prepare-template-example: prepare-template-default
	$(eval FLAVOUR=example)
	cp -f -r $(ROOT_FOLDER)/component/template/flavour/example/* $(BUILD_FOLDER)

prepare-template-aws: prepare-template-default
	$(eval FLAVOUR=aws)
	cp -f -r $(ROOT_FOLDER)/component/template/flavour/$(FLAVOUR)/* $(BUILD_FOLDER)

##########################
#
# These are the targets to be invoked by the user to create a template-image
#
##########################

build-template-example: prepare-template-example build-template-internal

build-template-aws: prepare-template-aws build-template-internal

build-template: prepare-template-default build-template-internal

run-template:
	touch $(TEMPLATE_ENV_FILE)
	docker run \
		-d \
		-v $(TEMPLATE_PERSISTENT)/builds:/var/template_home/builds \
		-v $(TEMPLATE_PERSISTENT)/jobs:/var/template_home/jobs \
		-v $(TEMPLATE_PERSISTENT)/users:/var/template_home/users \
		-v $(TEMPLATE_PERSISTENT)/logs:/var/template_home/logs \
		--env-file $(TEMPLATE_ENV_FILE) \
		-p 8080:$(TEMPLATE) \
		$(FRAMEWORK_PREFIX)/template-$(FLAVOUR):$(VERSION)

help-template:
	@cat $(ROOT_FOLDER)/component/template/help.md

