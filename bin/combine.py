import os
import argparse
import shutil


def get_args():
    parser = argparse.ArgumentParser(description="Add partition to folder. Expected to run on instance.")
    parser.add_argument('--target', default="builds", help='target folder')
    parser.add_argument('--component', default=None, required=True, help='Component')
    parser.add_argument('--flavours', default="default", help='flavours to combine')
    args = parser.parse_args()
    return args

def from_template(template, destination):
    with open(template, "r") as template_file:
        with open(destination, "w") as destination_file:
            for line in template_file:
                for key in os.environ.keys():
                    if key[:4] == "JAM_":
                        line = line.replace("${" + key + "}", os.environ[key])
                destination_file.write(line)

def copy_folder(src, dst):
    for (root, folders, files) in os.walk(src):
        destination_dir = os.path.join(dst, root[len(src)+1:])
        if not os.path.exists(destination_dir):
            os.makedirs(destination_dir)
        for file_name in files:
            src_file = os.path.join(root,file_name)
            if file_name[-9:] == ".template":
                destination_file = os.path.join(destination_dir, file_name[:-9])
                from_template(src_file, destination_file)
            else:
                destination_file = os.path.join(destination_dir, file_name)
                shutil.copyfile(src_file, destination_file)



def main():
    args = get_args()
    src = os.path.join("component", "template", "flavour", "default")
    for flavour in args.flavours.split(","):
        src = os.path.join("component", args.component, "flavour", flavour)
        copy_folder(src, args.target)


main()